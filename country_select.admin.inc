<?php

/**
 * Implements hook_admin()
 *
 * @return $form array
 */
function country_select_admin() {
    $form = array();
    $form['country_select_dropdown_block_label'] = array(
        '#type' => 'textfield',
        '#title' => t('Dropdown block label'),
        '#description' => t('The label used on the dropdown block selector.'),
        '#size' => 60,
        '#required' => TRUE,
        '#default_value' => variable_get('country_select_dropdown_block_label', 'Location'),
    );
    $form['country_select_dropdown_block_submit'] = array(
        '#type' => 'textfield',
        '#title' => t('Dropdown block submit text'),
        '#description' => t('The text used on the dropdown block submit.'),
        '#size' => 60,
        '#required' => TRUE,
        '#default_value' => variable_get('country_select_dropdown_block_submit', 'Save'),
    );
    $form['country_select_link_block_label'] = array(
        '#type' => 'textfield',
        '#title' => t('Link block label'),
        '#description' => t('The label used on the link block text.'),
        '#size' => 60,
        '#required' => TRUE,
        '#default_value' => variable_get('country_select_link_block_label', 'Location'),
    );
    $form['country_select_link_block_destination'] = array(
        '#type' => 'textfield',
        '#title' => t('Link block destination url'),
        '#description' => t('The destination url for the link block.'),
        '#size' => 60,
        '#required' => TRUE,
        '#default_value' => variable_get('country_select_link_block_destination', 'country_select'),
    );
    $form['country_select_country_order'] = array(
        '#type' => 'textfield',
        '#title' => t('Country ordering'),
        '#description' => t('Enter a comma separated list of country codes to move them to the top of the list.<br/>Invalid codes will automatically be sanitised.'),
        '#size' => 60,
        '#default_value' => variable_get('country_select_country_order', 'AU, NZ, JP, GB, CA, US'),
    );
    $form['country_select_country_order_global'] = array(
        '#type' => 'checkbox',
        '#title' => t('Make ordering global.'),
        '#description' => t('Apply the above country ordering to Drupal\'s country list.'),
        '#default_value' => variable_get('country_select_country_order_global', FALSE),
    );

    return system_settings_form($form);
}

/**
 * Implements hook_validate()
 *
 * Admin form validation and sanitation.
 *
 * @param $form
 * @param $form_state
 */
function country_select_admin_validate($form, &$form_state) {
    $country_order = $form_state['values']['country_select_country_order'];
    if($country_order) {
        // Use the predefined list as out own hook already alters country_get_list().
        include_once DRUPAL_ROOT . '/includes/iso.inc';
        $countries = _country_get_predefined_list();

        $country_order = preg_replace('/\s+/', '', $country_order);
        $keys = explode(',', $country_order);
        $keys = array_combine($keys, $keys);

        // Only keep keys if they are valid country codes.
        $key_result = array_intersect_key($countries, $keys);

        // The above result is in the wrong order however. Return it to the order of the user input.
        $final_order = array_replace($keys, $key_result);
        $final_order = array_intersect_key($final_order, $key_result);

        if($key_result){
            $form_state['values']['country_select_country_order'] = implode(", ",array_keys($final_order));

        } else {
            $form_state['values']['country_select_country_order'] = FALSE;
        }
    }
}
