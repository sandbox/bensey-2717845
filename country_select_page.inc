<?php
function country_select_page(){
    $content = '';
    $content .= 'Please select your country below.';

    $form = drupal_get_form('country_select_country_selector_form');
    $content .= drupal_render($form);
    
    return $content;
}