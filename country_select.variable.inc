<?php
/**
 * @file
 * Variable API module. Definition for some xample variables
 */

/**
 * Implements hook_variable_group_info()
 */
function country_select_variable_group_info() {
  // Group for variable that have no group
//  $groups['commerce_ajax_cart_link'] = array(
//      'title' => t('Commerce Ajax Cart Link'),
//      'description' => t("Variables used in Commerce Ajax Cart Link."),
//      'path' => array('admin/commerce/config/advanced-settings/commerce_ajax_cart_link'),
//  );
//  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function country_select_variable_info($options) {
//  $variables['commerce_ajax_cart_link_block_link_text'] = array(
//      'title' => t('Block link text'),
//      'type' => 'string',
//      'default' => 'View cart',
//      'group' => 'commerce_ajax_cart_link',
//  );
//  $variables['commerce_ajax_cart_link_empty_text'] = array(
//      'title' => t('Empty cart text'),
//      'type' => 'string',
//      'default' => 'empty',
//      'group' => 'commerce_ajax_cart_link',
//  );
//  $variables['commerce_ajax_cart_link_submit_action'] = array(
//      'title' => t('Add to cart submit action'),
//      'type' => 'select',
//      'options' => array(
//          'none' => t('No action'),
//          'replace' => t('Replace with message'),
//          'append' => t('Append message'),
//      ),
//      'default' => 'replace',
//      'group' => 'commerce_ajax_cart_link',
//  );
//  $variables['commerce_ajax_cart_link_replace_submit_text'] = array(
//      'title' => t('Submit replacement message'),
//      'type' => 'string',
//      'default' => 'Item added to your cart',
//      'group' => 'commerce_ajax_cart_link',
//  );
//  $variables['commerce_ajax_cart_link_default_messages'] = array(
//      'title' => t('Return default system messages'),
//      'type' => 'boolean',
//      'default' => FALSE,
//      'group' => 'commerce_ajax_cart_link',
//  );
//  return $variables;
}